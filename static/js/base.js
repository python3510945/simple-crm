  function updateClock() {
        const now = new Date();
        const hours = now.getHours().toString().padStart(2, '0');
        const minutes = now.getMinutes().toString().padStart(2, '0');
        const seconds = now.getSeconds().toString().padStart(2, '0');

        const clockElement = document.getElementById('clock');
        clockElement.textContent = `${hours}:${minutes}:${seconds}`;
    }

    updateClock();
    setInterval(updateClock, 1000);

    document.addEventListener('DOMContentLoaded', function () {
        const callTimeInput = document.getElementById('callTime');
        const now = new Date();
        const formattedTime = now.toLocaleTimeString();
        callTimeInput.value = formattedTime;
    });

     document.addEventListener('DOMContentLoaded', function () {
        const inserted_id = "{{ inserted_id }}";
         if (inserted_id) {
            $('#successModal').modal('show');
        }
    });

     setTimeout(function() {
                document.getElementById("flash-message").style.display = "none";
            }, 5000);


     document.querySelectorAll('.edit-btn').forEach(button => {
    button.addEventListener('click', function() {
        const clientId = this.getAttribute('data-id');
        const fullName = this.getAttribute('data-fullname');
        const managerId = this.getAttribute('data-manager-id');
        const managerName = this.getAttribute('data-manager-name');

        document.getElementById('client_fullName').value = fullName;
        document.getElementById('client_ID').value = clientId;
        document.getElementById('client_manager_name').value = managerName;
        document.getElementById('manager_ID').value = managerId;

        $('#makeClients').modal('show');
    });
});

//      document.addEventListener('DOMContentLoaded', function () {
//     const editButtons = document.querySelectorAll('.edit-btn');
//
//     editButtons.forEach(button => {
//         button.addEventListener('click', function () {
//             console.log(button.getAttribute('data-client'))
//             const clientData = button.getAttribute('data-client');
//
//             document.getElementById('ecallTime').value = clientData['call_duration'];
//             document.getElementById('efullName').value = clientData['full_name'];
//             document.getElementById('ephone').value = clientData['phone'];
//             document.getElementById('eemail').value = clientData['email'];
//             document.getElementById('emanager').value = clientData['manager_ID'];
//             document.getElementById('emarketingResource').value = clientData['marketing_resourse_ID'];
//             document.getElementById('eengagement').value = clientData['engagement'];
//
//             $('#editModal').modal('show');
//         });
//     });
// });
