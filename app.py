import copy
import secrets
from datetime import datetime

from flask import Flask, render_template, url_for, request, flash, redirect

from model.maintenance import Maintenance

app = Flask(__name__)
app.secret_key = '2f4405934ce69faff2a633cafd9a44471dbdccfdcab7466724f392fc1ddab89c'

crm = Maintenance()


@app.route('/', methods=['POST', 'GET'])
@app.route('/home', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        fullName = request.form['fullName']
        phone = request.form['phone']
        email = request.form['email']
        managerID = request.form['manager']
        marketingResourceID = request.form['marketingResource']
        callTime = request.form['callTime']
        engagement = request.form['engagement']

        call_time = datetime.strptime(callTime, '%H:%M:%S')
        current_time = datetime.now()
        call_duration = (current_time - call_time).seconds

        data = {
            "full_name": fullName,
            "phone": phone,
            "email": email,
            "call_duration": call_duration,
            "manager_ID": managerID,
            "marketing_resourse_ID": marketingResourceID,
            "engagement": engagement
        }
        inserted_id = crm.calls().insert_call_data(data)
        flash(f"Клiент з ID '{inserted_id}' успiшно доданий до бази!.")
        return redirect('/')
    else:
        clients_data = crm.calls().get_all_calls()
        clients = crm.manage_client_list(clients_data)
        marketing = crm.marketing().get_all_resources()
        users = crm.managers().get_all_users()
        offers = crm.offers().get_all_offerss()
        courses = crm.courses().get_all_courcess()

        # for cl in clients:
        #     print(cl)
        return render_template('index.html',
                               clients=clients,
                               marketing=marketing,
                               users=users,
                               offers=offers,
                               courses=courses)


@app.route('/about')
def about():
    return render_template('about.html')


@app.route('/clients', methods=['POST', 'GET'])
def clients():
    if request.method == 'POST':
        client_ID = request.form['client_ID']
        client_fullName = request.form['client_fullName']
        manager_ID = request.form['manager_ID']
        courses_ID = request.form['courses']
        offers_ID = request.form['offers']

        client_data = {
            "offers_ID": offers_ID,
            "calls_ID": client_ID,
            "manager_id": manager_ID,
            "balance": 0,
            "cource_ID": courses_ID
        }


        inserted_id = crm.clients().insert_client_data(copy.deepcopy(client_data))
        flash(f"Договiр для клiента: '{inserted_id}': '{client_fullName}' успiшно оформлено!.")
        return redirect('/clients')
    else:
        clients_data = crm.clients().get_all_clients()
        print(clients_data)
        clients = crm.manage_student_list(clients_data)


        return render_template('clients.html', clients=clients)


@app.route('/client_details/<string:id>')
def client_details(id):
    clients_data = crm.clients().get_client_by_id(id)
    student_data = crm.manage_student_details(clients_data)
    return render_template('client_details.html',
                           student_data=student_data)


@app.route('/reports')
def reports():
    return render_template('reports.html')


@app.route('/calls')
def calls():
    clients_data = crm.calls().get_all_calls()
    clients = crm.manage_client_list(clients_data)
    marketing = crm.marketing().get_all_resources()
    users = crm.managers().get_all_users()
    offers = crm.offers().get_all_offerss()
    courses = crm.courses().get_all_courcess()

    return render_template('calls.html',
                           clients=clients,
                           marketing=marketing,
                           users=users,
                           offers=offers,
                           courses=courses)


# @app.route('/user/<string:name>/<int:id>')
# def user(name, id):
#     return "user, page !" + name + " - " + str(id)


if __name__ == '__main__':
    app.run(debug=True)
