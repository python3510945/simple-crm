from datetime import datetime

from bson import ObjectId
from pymongo import MongoClient


class TeachersCollection:
    def __init__(self, connection, db_name, collection_name):
        self.teachers = connection
        self.db = self.teachers[db_name]
        self.collection = self.db[collection_name]

    def insert_teachers_data(self, data_dict):
        data_dict["creation_date"] = datetime.now()
        result = self.collection.insert_one(data_dict)
        return result.inserted_id

    def get_teachers_by_id(self, teachers_id):
        teachers_id_obj = ObjectId(teachers_id)
        return self.collection.find_one({"_id": teachers_id_obj})

    def get_all_teacherss(self):
        return list(self.collection.find({}))

    def delete_teachers_by_id(self, teachers_id):
        result = self.collection.delete_one({"_id": teachers_id})
        return result.deleted_count > 0

    def update_teachers_by_id(self, teachers_id, update_dict):
        result = self.collection.update_one({"_id": teachers_id}, {"$set": update_dict})
        return result.modified_count > 0


# Пример использования класса
# if __name__ == "__main__":
#     teachers_crm = teachersCRM("mongodb://localhost:27017/", "SimpleCRM", "teacherss")
#
#     teachers_data = {
#         "offers_ID": "offer123",
#         "calls_ID": "call456",
#         "balance": 1000,
#         "cource_ID": "cource789"
#     }
#
#     inserted_id = teachers_crm.insert_teachers_data(teachers_data)
#     print("Inserted ID:", inserted_id)
#
#     teachers = teachers_crm.get_teachers_by_id(inserted_id)
#     print("Retrieved teachers:", teachers)
#
#     all_teacherss = teachers_crm.get_all_teacherss()
#     print("All teacherss:", all_teacherss)
#
#     # Continue with delete_teachers_by_id, update_teachers_by_id methods...
