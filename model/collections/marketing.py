from pymongo import MongoClient
from bson.objectid import ObjectId


class MarketingResourceCollection:
    def __init__(self, connection, db_name, collection_name):
        self.client = connection
        self.db = self.client[db_name]
        self.collection = self.db[collection_name]

    def get_resource(self, resource_id):
        resource = self.collection.find_one({'_id': ObjectId(resource_id)})
        return resource

    def get_all_resources(self):
        resources = list(self.collection.find())
        return resources

    def update_resource(self, resource_id, update_data):
        result = self.collection.update_one({'_id': ObjectId(resource_id)}, {'$set': update_data})
        return result.modified_count

    def add_resource(self, resource_data):
        result = self.collection.insert_one(resource_data)
        return str(result.inserted_id)

    def delete_resource(self, resource_id):
        result = self.collection.delete_one({'_id': ObjectId(resource_id)})
        return result.deleted_count
