from datetime import datetime

from bson import ObjectId
from pymongo import MongoClient


class ClientCollection:
    def __init__(self, connection, db_name, collection_name):
        self.client = connection
        self.db = self.client[db_name]
        self.collection = self.db[collection_name]

    def insert_client_data(self, data_dict):
        data_dict["creation_date"] = data_dict.get("creation_date", datetime.now())
        result = self.collection.insert_one(data_dict)
        return result.inserted_id

    def get_client_by_id(self, client_id):
        client_id_obj = ObjectId(client_id)
        return self.collection.find_one({"_id": client_id_obj})

    def get_all_clients(self):
        data = list(self.collection.find({}).sort("creation_date", -1))
        return data

    def delete_client_by_id(self, client_id):
        result = self.collection.delete_one({"_id": client_id})
        return result.deleted_count > 0

    def update_client_by_id(self, client_id, update_dict):
        result = self.collection.update_one({"_id": client_id}, {"$set": update_dict})
        return result.modified_count > 0


# Пример использования класса
# if __name__ == "__main__":
#     client_crm = ClientCRM("mongodb://localhost:27017/", "SimpleCRM", "clients")
#
#     client_data = {
#         "offers_ID": "offer123",
#         "calls_ID": "call456",
#         "balance": 1000,
#         "cource_ID": "cource789"
#     }
#
#     inserted_id = client_crm.insert_client_data(client_data)
#     print("Inserted ID:", inserted_id)
#
#     client = client_crm.get_client_by_id(inserted_id)
#     print("Retrieved client:", client)
#
#     all_clients = client_crm.get_all_clients()
#     print("All clients:", all_clients)
#
#     # Continue with delete_client_by_id, update_client_by_id methods...
