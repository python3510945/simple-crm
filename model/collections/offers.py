from datetime import datetime
from pymongo import MongoClient


class OffersCollection:
    def __init__(self, connection, db_name, collection_name):
        self.offers = connection
        self.db = self.offers[db_name]
        self.collection = self.db[collection_name]

    def insert_offers_data(self, data_dict):
        data_dict["creation_date"] = datetime.now()
        result = self.collection.insert_one(data_dict)
        return result.inserted_id

    def get_offers_by_id(self, offers_id):
        return self.collection.find_one({"_id": offers_id})

    def get_all_offerss(self):
        return list(self.collection.find({}))

    def delete_offers_by_id(self, offers_id):
        result = self.collection.delete_one({"_id": offers_id})
        return result.deleted_count > 0

    def update_offers_by_id(self, offers_id, update_dict):
        result = self.collection.update_one({"_id": offers_id}, {"$set": update_dict})
        return result.modified_count > 0


# Пример использования класса
# if __name__ == "__main__":
#     offers_crm = offersCRM("mongodb://localhost:27017/", "SimpleCRM", "offerss")
#
#     offers_data = {
#         "offers_ID": "offer123",
#         "calls_ID": "call456",
#         "balance": 1000,
#         "cource_ID": "cource789"
#     }
#
#     inserted_id = offers_crm.insert_offers_data(offers_data)
#     print("Inserted ID:", inserted_id)
#
#     offers = offers_crm.get_offers_by_id(inserted_id)
#     print("Retrieved offers:", offers)
#
#     all_offerss = offers_crm.get_all_offerss()
#     print("All offerss:", all_offerss)
#
#     # Continue with delete_offers_by_id, update_offers_by_id methods...
