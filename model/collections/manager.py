from pymongo import MongoClient
from bson.objectid import ObjectId


class UsersCollection:
    def __init__(self, connection, db_name, collection_name):
        self.client = connection
        self.db = self.client[db_name]
        self.collection = self.db[collection_name]

    def get_user(self, user_id):
        user = self.collection.find_one({'_id': ObjectId(user_id)})
        return user

    def get_all_users(self):
        users = list(self.collection.find())
        return users

    def update_user(self, user_id, update_data):
        result = self.collection.update_one({'_id': ObjectId(user_id)}, {'$set': update_data})
        return result.modified_count

    def add_user(self, user_data):
        result = self.collection.insert_one(user_data)
        return str(result.inserted_id)

    def delete_user(self, user_id):
        result = self.collection.delete_one({'_id': ObjectId(user_id)})
        return result.deleted_count
