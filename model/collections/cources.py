from datetime import datetime

from bson import ObjectId
from pymongo import MongoClient


class СourcesCollection:
    def __init__(self, connection, db_name, collection_name):
        self.cources = connection
        self.db = self.cources[db_name]
        self.collection = self.db[collection_name]

    def insert_cources_data(self, data_dict):
        data_dict["creation_date"] = datetime.now()
        result = self.collection.insert_one(data_dict)
        return result.inserted_id

    def get_cources_by_id(self, cources_id):
        cources_id_obj = ObjectId(cources_id)
        return self.collection.find_one({"_id": cources_id_obj})

    def get_all_courcess(self):
        return list(self.collection.find({}))

    def delete_cources_by_id(self, cources_id):
        cources_id_obj = ObjectId(cources_id)
        result = self.collection.delete_one({"_id": cources_id_obj})
        return result.deleted_count > 0

    def update_cources_by_id(self, cources_id, update_dict):
        result = self.collection.update_one({"_id": cources_id}, {"$set": update_dict})
        return result.modified_count > 0


# Пример использования класса
# if __name__ == "__main__":
#     cources_crm = courcesCRM("mongodb://localhost:27017/", "SimpleCRM", "courcess")
#
#     cources_data = {
#         "offers_ID": "offer123",
#         "calls_ID": "call456",
#         "balance": 1000,
#         "cource_ID": "cource789"
#     }
#
#     inserted_id = cources_crm.insert_cources_data(cources_data)
#     print("Inserted ID:", inserted_id)
#
#     cources = cources_crm.get_cources_by_id(inserted_id)
#     print("Retrieved cources:", cources)
#
#     all_courcess = cources_crm.get_all_courcess()
#     print("All courcess:", all_courcess)
#
#     # Continue with delete_cources_by_id, update_cources_by_id methods...
