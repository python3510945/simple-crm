from datetime import datetime

from bson import ObjectId
from pymongo import MongoClient


class CallsCollection:
    def __init__(self, connection, db_name, collection_name):
        self.client = connection
        self.db = self.client[db_name]
        self.collection = self.db[collection_name]

    def insert_call_data(self, data_dict):
        data_dict["creation_date"] = datetime.now()
        result = self.collection.insert_one(data_dict)
        return result.inserted_id

    def get_call_by_id(self, call_id):
        call_id_obj = ObjectId(call_id)
        return self.collection.find_one({"_id": call_id_obj})

    def get_calls_by_date(self, start_date, end_date):
        return list(self.collection.find({
            "creation_date": {"$gte": start_date, "$lte": end_date}
        }))

    def get_calls_by_name(self, full_name):
        return list(self.collection.find({"full_name": full_name}))

    def get_calls_by_phone(self, phone):
        return list(self.collection.find({"phone": phone}))

    def get_calls_by_manager(self, manager_ID):
        return list(self.collection.find({"manager_ID": manager_ID}))

    def delete_call_by_id(self, call_id):
        result = self.collection.delete_one({"_id": call_id})
        return result.deleted_count > 0

    def update_call_by_id(self, call_id, update_dict):
        result = self.collection.update_one({"_id": call_id}, {"$set": update_dict})
        return result.modified_count > 0

    def get_all_calls(self):
        return list(self.collection.find({}).sort("creation_date", -1))

# # Пример использования класса
# if __name__ == "__main__":
#     crm = SimpleCRM("mongodb://localhost:27017/", "SimpleCRM", "callsData")
#
#     data = {
#         "full_name": "John Doe",
#         "phone": "123-456-7890",
#         "email": "john@example.com",
#         "call_duration": 120,
#         "manager_ID": "manager123",
#         "marketing_resourse_ID": "marketing456",
#         "engagement": 8
#     }
#
#     inserted_id = crm.insert_call_data(data)
#     print("Inserted ID:", inserted_id)
#
#     call = crm.get_call_by_id(inserted_id)
#     print("Retrieved call:", call)
#
#     calls_by_date = crm.get_calls_by_date(datetime(2023, 1, 1), datetime(2023, 8, 1))
#     print("Calls by date range:", calls_by_date)
#
#     calls_by_name = crm.get_calls_by_name("John Doe")
#     print("Calls by name:", calls_by_name)
#
#     # Continue with other methods...
