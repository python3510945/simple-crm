from pymongo import MongoClient


class Connector(object):
    def __init__(self):
        self.client = MongoClient('mongodb://localhost:27017/')

    def connect(self):
        return self.client

