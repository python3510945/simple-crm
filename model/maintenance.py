from datetime import timedelta, datetime

from model.collections.calls import CallsCollection
from model.collections.clients import ClientCollection
from model.collections.cources import СourcesCollection
from model.collections.manager import UsersCollection
from model.collections.marketing import MarketingResourceCollection
from model.collections.offers import OffersCollection
from model.collections.teachers import TeachersCollection
from model.connector import Connector


class Maintenance(object):
    def __init__(self):
        self.__dbName = 'SimleCRM'
        conn = Connector().connect()
        self.__calls = CallsCollection(conn, self.__dbName, 'callsData')
        self.__clients = ClientCollection(conn, self.__dbName, 'clients')
        self.__marketing = MarketingResourceCollection(conn, self.__dbName, 'marketing_resource')
        self.__managers = UsersCollection(conn, self.__dbName, 'users')
        self.__courses = СourcesCollection(conn, self.__dbName, 'cources')
        self.__offers = OffersCollection(conn, self.__dbName, 'offers')
        self.__teachers = TeachersCollection(conn, self.__dbName, 'teachers')

    def calls(self):
        return self.__calls

    def clients(self):
        return self.__clients

    def marketing(self):
        return self.__marketing

    def managers(self):
        return self.__managers

    def courses(self):
        return self.__courses

    def offers(self):
        return self.__offers

    def teachers(self):
        return self.__teachers

    def manage_client_list(self, data):
        for dt in data:
            mdata = self.__managers.get_user(dt['manager_ID'])
            rdata = self.__marketing.get_resource(dt['marketing_resourse_ID'])
            manager_data = {'manager_ID': dt['manager_ID'], 'manager_Name': mdata['full_name']}
            marketing_data = {'marketing_resourse_ID': dt['marketing_resourse_ID'],
                              'marketing_resourse_name': rdata['name']}
            call_duration_seconds = dt['call_duration']
            call_duration_time = timedelta(seconds=call_duration_seconds)
            call_duration = str(call_duration_time)
            dt['call_duration'] = call_duration
            dt['manager_ID'] = manager_data
            dt['marketing_resourse_ID'] = marketing_data
            dat = dt['creation_date']
            formatted_dt = dat.strftime('%Y-%m-%d %H:%M:%S')
            dt['creation_date'] = formatted_dt

        return data

    def manage_student_list(self, clients_data):
        data = []
        for dt in clients_data:
            students_data = self.__calls.get_call_by_id(dt['calls_ID'])
            course_data = self.__courses.get_cources_by_id(dt['cource_ID'])
            students = {}
            students['ID'] = dt['_id']
            students['student_name'] = students_data['full_name']
            students['course_name'] = course_data['name']
            dat = dt['creation_date']
            formatted_dt = dat.strftime('%Y-%m-%d %H:%M:%S')
            students['offers_date'] = formatted_dt
            students['balance'] = dt['balance'] / 100
            data.append(students)

        return data

    def manage_student_details(self, clients_data):
        students = {}
        students_data = self.__calls.get_call_by_id(clients_data['calls_ID'])
        course_data = self.__courses.get_cources_by_id(clients_data['cource_ID'])
        teacher_data = self.__teachers.get_teachers_by_id(course_data['teacher_ID'])
        students['ID'] = clients_data['_id']
        students['student_data'] = students_data
        students['course_data'] = course_data
        students['teacher_data'] = teacher_data
        dat = clients_data['creation_date']
        formatted_dt = dat.strftime('%Y-%m-%d %H:%M:%S')
        students['offers_date'] = formatted_dt
        students['balance'] = clients_data['balance'] / 100

        return students
    #
    #     call = crm.get_call_by_id(inserted_id)
    #     print("Retrieved call:", call)
    #
    #     calls_by_date = crm.get_calls_by_date(datetime(2023, 1, 1), datetime(2023, 8, 1))
    #     print("Calls by date range:", calls_by_date)
    #
    #     calls_by_name = crm.get_calls_by_name("John Doe")
    #     print("Calls by name:", calls_by_name)

    # def insert_toCollection_callData(self, data_dict):
    #     calls_collection = self.__db["callsData"]
    # 
    #     call_data = {
    #         "creation_date": datetime.now(),
    #         "full_name": data_dict["full_name"],
    #         "phone": data_dict["phone"],
    #         "email": data_dict["email"],
    #         "call_duration": data_dict["call_duration"],
    #         "manager_ID": data_dict["manager_ID"],
    #         "marketing_resourse_ID": data_dict["marketing_resourse_ID"],
    #         "engagement": data_dict["engagement"]
    #     }
    # 
    #     result = calls_collection.insert_one(call_data)
    # 
    #     if result.inserted_id:
    #         return True
    #     else:
    #         return False
    # 
    # # def get_one(self):
    # #     self.__db.
